#include <iostream>
#include <utility>
#include <cassert>
#include <sys/time.h>

#include "boost/pfr.hpp"

namespace detail {

/**
 * Convert argument to string.
 * boost::pfr::detail::print_impl will be used to convert the arg to string if the arg is POD
 * @tparam T - argument type [*** must be rvalue reference or lvalue ***]
 *             (lvalue reference cannot be compiled) else there will compile error
 * @param arg - input argument
 * @return argument as string
 */
template<typename T>
std::string _ToString(T &&arg) {
  using namespace boost::pfr::ops;
  std::stringstream ss;
  ss << arg;
  return ss.str();
}

/**
 * Put the argument to format string
 * @param last_processed_pos - position of the part which should be processed
 * @param format - format string
 * @param arg - input argument
 * @return - a position of the unprocessed part
 *           (position of the part which should be processed on next iteration)
 */
std::size_t _PutArgument(std::size_t last_processed_pos, std::string &format, std::string &&arg) {
  std::size_t in_position = format.find_first_of('%', last_processed_pos);
  // check if there is '%'
  assert(in_position != std::string::npos);

  format.erase(in_position, 1);
  // TODO use std::move(arg) if there is insert(T &&arg) implementation
  format.insert(in_position, arg);
  return in_position + arg.size();
}

/**
 * Process argument: put the arg (as string) to format string.
 * In this case we have the last argument
 * @tparam T - argument type [*** must not be a pointer***] else there will compile error
 * @param last_processed_pos - position of the part which should be processed
 * @param format - format string
 * @param arg - last input argument
 */
template<typename T>
void _ProcessArguments(std::size_t last_processed_pos, std::string &format, T &&arg) {
  _PutArgument(
          last_processed_pos,
          format,
          _ToString(std::move(arg)) // use std::move to make a rvalue reference (required by _ToString)
  );
}

/**
 * Process arguments: put the arg (as string) to format string
 * and call itself to separate the first element from the remaining arguments and continue processing
 * @tparam T - separated argument type
 * @tparam Types - types of the remaining arguments
 * @param last_processed_pos - position of the part which should be processed
 * @param format - format string
 * @param arg - separated argument
 * @param args - remaining arguments
 */
template<typename T, typename ... Types>
void _ProcessArguments(std::size_t last_processed_pos, std::string &format, T &&arg, Types &&... args) {
  // put the separated argument to format string
  last_processed_pos = _PutArgument(
          last_processed_pos,
          format,
          _ToString(std::move(arg)) // use std::move to make a rvalue reference (required by _ToString)
  );

  // continue processing
  _ProcessArguments(last_processed_pos, format, std::forward<Types>(args)...);
}

/**
 * Process arguments: call the _ProcessArguments (another signature) to separate the first element from the arguments
 * @tparam Types - Types of the input arguments
 * @param format - format string
 * @param args - input arguments
 */
template<typename ... Types>
void _ProcessArguments(std::string &format, Types &&... args) {
  _ProcessArguments(0, format, std::forward<Types>(args)...);
}

}

/**
* Puts input arguments into the template string
* @tparam Types - input argument types
* @param format - template string
* @param args - input arguments
* @return - result formatted string
*/
template<typename ... Types>
std::string Format(std::string format, Types &&... args) {
  detail::_ProcessArguments(format, std::forward<Types>(args)...);
  return format;
}

/**
* The function used when there are no input arguments
* @param format - format string
* @return - the format string
*/
inline std::string Format(const std::string &format) {
  return format;
}


struct UserInfo {
  int m_id = 0;
  std::string m_name;
  int m_age = 0;
  char m_gender = 'm';
};

int main(int argc, char *argv[]) {
  UserInfo user{203214, "John", 27, 'm'};
  /* ... */
  std::cout << Format("User % is logged in", user) << std::endl;
}
