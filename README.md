# Format string using template metaprogramming and boost::pfr

### Motivating example
```c++
struct UserInfo {
  int m_id = 0;
  std::string m_name;
  int m_age = 0;
  char m_gender = 'm';
};

int main(int argc, char *argv[]) {
  UserInfo user{203214, "John", 27, 'm'};
  /* ... */
  std::cout << Format("User % is logged in", user) << std::endl;
}
```
Outputs:
```
User {203214, John, 27, m} is logged in
```

The project is an example of using the
boost::pfr library (https://github.com/apolukhin/magic_get | author - Antony Polukhin)
and implementation of simple variadic templates and pattern matching